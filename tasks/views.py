from django.shortcuts import render, redirect
from tasks.forms import CreateTaskForm
from tasks.models import Task
from django.contrib.auth.decorators import login_required


# Create your views here.
def create_task(request):
    if request.method == "POST":
        form = CreateTaskForm(request.POST)
        if form.is_valid():
            form.save()
        return redirect("list_projects")
    else:
        form = CreateTaskForm()
    context = {"create_task": form}
    return render(request, "tasks/create_task.html", context)


@login_required
def show_my_tasks(request):
    show_my_tasks = Task.objects.filter(assignee=request.user)
    context = {"show_my_tasks": show_my_tasks}
    return render(request, "tasks/show_my_tasks.html", context)
